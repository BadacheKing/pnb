﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication2
{
    class Manager
    {
        Notebook _notebook;

        public Manager()
        {
            _notebook = new Notebook();
            _notebook.Add("Ivanov1", "Ivan1", "Ivanovovic1", "89212245321", "Russia1", new DateTime(1992,7,30));
            _notebook.Add("Ivanov2", "Ivan2", "Ivanovovic2", "89212245322", "Russia2", new DateTime(1995, 3, 3));
            _notebook.Add("Ivanov3", "Ivan3", "Ivanovovic3", "89212245323", "Russia3", new DateTime(1967, 8, 8));
            _notebook.Add("Ivanov4", "Ivan4", "Ivanovovic4", "89212245324", "Russia4", new DateTime(1977, 2, 23));
            
        }

        public void ShowMenu()
        {
            
            Console.WriteLine("Выберите нужное действие:\n");

            Console.WriteLine("1 - Показать все записи");
            Console.WriteLine("2 - Добавить новую запись");
            Console.WriteLine("3 - Удалить запись");
            Console.WriteLine("4 - Очистить все");

            Console.WriteLine();
            Console.WriteLine("5 - ВЫХОД");
            Console.WriteLine();

            int number = 0;
            string keystr = "";
            

            while (true)
            {
                keystr = Console.ReadLine();
                if (!int.TryParse(keystr, out number))
                {
                    ShowMessage("Ошибка! Повторите ввод"); 
                }
                else break;
            }

            AnalizeSelectedMenu(number);
            Console.ReadLine();
        }

        void AnalizeSelectedMenu(int number)
        {
        
            
            switch (number)
            {
                case 1: Show();
                    break;

                case 2: Add();
                    break;

                case 3: Remove();
                    break;

                case 4: ClearAll();
                    break;

                case 5: Environment.Exit(5);
                    break;
            }
           
            Console.WriteLine();
            ShowMenu();
        }



        bool AskResult(string message)
        {
            message += "\n y/n?";
            Console.WriteLine(message);
            string result = Console.ReadLine();
            if (result == "y") return true;
            else return false;
        }

        void Show()
        {
            if (_notebook.Count() == 0) 
            {
                Console.WriteLine("Записная книга пуста!");
                return;
            } 
            Console.WriteLine(_notebook.ToString());
            Console.WriteLine();
        }

        void Add()
        {
            Console.WriteLine("Введите фамилию:");
            string surname = Console.ReadLine();

            Console.WriteLine("Введите имя:");
            string name = Console.ReadLine();

            Console.WriteLine("Введите отчество:");
            string otch = Console.ReadLine();

            Console.WriteLine("Введите страну рождения:");
            string country = Console.ReadLine();

            Console.WriteLine("Введите дату рождения:");
            string brthday = Console.ReadLine();
            DateTime bzday = Convert.ToDateTime(brthday);

            Console.WriteLine("Введите телефон:");
            string phone = Console.ReadLine();

            _notebook.Add(surname, name, otch, phone, country, bzday);
        }

        void Remove()
        {
            if (_notebook.Count() == 0) 
            {
                Console.WriteLine("Записная книга пуста!");
                return;
            }
            Show();
            Console.WriteLine("Выберите id записи, которую необходимо удалить:");
            
            long id = -1;
            string sid = "";

            while (true)
            {
                sid = Console.ReadLine();
                if (!long.TryParse(sid, out id))
                {
                    ShowMessage("Ошибка! Повторите ввод"); 
                }
                else break;
            }

            if (_notebook.Remove(id))
            {                
                ShowMessage("Удаление прошло успешно!");      
            }
            else
            {
                ShowMessage("id не найден");                
            }
     

        }

        void ClearAll()
        {
            if (AskResult("Вы действительно хотите очистить записную книжку?"))
            _notebook.Clear();            

        }

        void ShowMessage(string message, ConsoleColor? textColor=null)
        {
            if (textColor != null)
            {
                var color = Console.ForegroundColor;
                Console.ForegroundColor = (ConsoleColor)textColor;
                Console.WriteLine(message);
                Console.ForegroundColor = color;
            }
            else Console.WriteLine(message);
        }
    }
}