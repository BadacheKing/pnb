﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication2
{
    public class Note 
    {
        private static long _id;

        public readonly long Id; // уникальный идентификатор записи
        public string Surname;
        public string Name;
        public string Otchestvo;
        public string PhoneNumber;
        public string Country;
        public DateTime Birthday;

        public Note(string surname, string name, string otchestvo, string phoneNumber, string country, DateTime birthday)
        {
            Id = _id++;
            Surname = surname;
            Name = name;
            Otchestvo = otchestvo;
            PhoneNumber = phoneNumber;
            Country = country;
            Birthday = birthday;
        }

        public override string ToString()
        {
            return string.Format("{6}: {0} {1} {2},\t{3} г/р,\t страна: {4},\t тел: {5}", Surname, Name, Otchestvo, Birthday.ToString("dd.MM.yyyy"), Country, PhoneNumber, Id); 
        }
    }
}
